Rails.application.routes.draw do
  namespace  :api do
    namespace  :v1 do
    resources :users, only: [:create]
    post '/login',to: 'users#login'
    post '/auto_login', to: 'users#auto_login'
    post '/price' , to: 'product#price'
    post '/products/create' ,  to: 'product#create'
    end
  end
 end
