class Api::V1::ProductController < ApplicationController
    def price
        @user=User.find(params[:user_id])
        render json: Product.all.map(&:to_object)
    end 

    def create
        @product=Product.find(params[:id])
        @product.picture.attach(params[:picture])
        render json: @product
    end
end