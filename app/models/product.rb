class Product < ApplicationRecord
    include Rails.application.routes.url_helpers
    has_one_attached :picture

    def to_object
        {
          id: id,
          name: name,
          price: price,
          unit: unit,
          currency: currency,
          uploaded: created_at,
          picture_url: url_for(picture),
        }
    end
end
